from django.contrib import admin

from .models import Client, Composant

admin.site.register(Client)
admin.site.register(Composant)
