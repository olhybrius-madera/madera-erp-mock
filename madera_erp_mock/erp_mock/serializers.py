from rest_framework import serializers

from .models import Client, Composant


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class ComposantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Composant
        fields = '__all__'
