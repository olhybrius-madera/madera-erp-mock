from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .models import Client, Composant
from .serializers import ClientSerializer, ComposantSerializer
from .services import MessageService


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        message_service = MessageService()
        serializer.save()
        json_data = JSONRenderer().render(serializer.data)
        message_service.envoyer_message('client_erp.cree', json_data)
        return Response(serializer.data)


class ComposantViewSet(viewsets.ModelViewSet):
    queryset = Composant.objects.all()
    serializer_class = ComposantSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        message_service = MessageService()
        serializer.save()
        json_data = JSONRenderer().render(serializer.data)
        message_service.envoyer_message('composant.cree', json_data)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_object(), data=request.data)
        serializer.is_valid(raise_exception=True)
        message_service = MessageService()
        serializer.save()
        json_data = JSONRenderer().render(serializer.data)
        message_service.envoyer_message('composant.maj', json_data)
        return Response(serializer.data)

