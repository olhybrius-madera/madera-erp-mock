import pika
from django.conf import settings


class MessageService:

    def __init__(self):
        credentials = pika.PlainCredentials(settings.BROKER_USER, settings.BROKER_PASSWORD)
        parameters = pika.ConnectionParameters(settings.BROKER_HOST,
                                               settings.BROKER_PORT,
                                               settings.BROKER_VHOST,
                                               credentials)
        connection = pika.BlockingConnection(parameters)
        self.channel = connection.channel()
        self.channel.exchange_declare(exchange='madera', exchange_type='topic', durable=True)

    def envoyer_message(self, routing_key, body):
        self.channel.basic_publish(exchange='madera', routing_key=routing_key, body=body)
