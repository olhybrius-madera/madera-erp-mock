from django.core.management.base import BaseCommand, CommandError
import pika
from django.conf import settings
from socket import gaierror
import time

class Command(BaseCommand):
    help = 'RabbitMQ consumer example for Madera app'

    def handle(self, *args, **options):
        credentials = pika.PlainCredentials(settings.BROKER_USER, settings.BROKER_PASSWORD)
        parameters = pika.ConnectionParameters(settings.BROKER_HOST,
                                       settings.BROKER_PORT,
                                       settings.BROKER_VHOST,
                                       credentials)
        subscriptions = settings.SUBSCRIPTIONS.split(',')

        while True:
            try:
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                channel.exchange_declare(exchange='madera', exchange_type='topic', durable=True)
                channel.queue_declare(queue=settings.BROKER_QUEUE, durable=True)

                for subscription in subscriptions:
                    channel.queue_bind(exchange='madera', queue=settings.BROKER_QUEUE, routing_key=subscription)

                print(' [*] Waiting for messages. To exit press CTRL+C')
                channel.basic_consume(queue=settings.BROKER_QUEUE, on_message_callback=self.callback)

                channel.start_consuming() 
            except (pika.exceptions.ConnectionClosed, pika.exceptions.AMQPConnectionError, gaierror):
                print('Broker unreachable, retrying to connect in', settings.BROKER_CONNECTION_RETRY_INTERVAL, 'seconds')
                time.sleep(settings.BROKER_CONNECTION_RETRY_INTERVAL)
                continue
            except KeyboardInterrupt:
                channel.stop_consuming()

            connection.close()
            break

    def callback(self, ch, method, properties, body):
        print(" [x] %r:%r" % (method.routing_key, body))
        ch.basic_ack(delivery_tag=method.delivery_tag)
