from django.apps import AppConfig


class ErpMockConfig(AppConfig):
    name = 'erp_mock'
