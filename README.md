# Madera Starter 

Ce projet sert de point de départ pour les microservices du projet Madera.

## Prérequis
Le seul logiciel nécessaire à l'utilisation du projet est [Docker](https://www.docker.com/).
Le projet requiert également qu'une instance de [madera-infra](https://gitlab.com/olhybrius-madera/madera-infra) soit en cours d'exécution.

## Dépendances
- [python 3.9](https://www.python.org/): langage dans lequel est écrit le projet
- [Django 3.1](https://github.com/django/django): framework de développement d'applications web
- [djangorestframework 3.9](https://github.com/encode/django-rest-framework): extension de Django pour le développement d'API RESTful
- [django-cors-headers 3.6](https://github.com/adamchainz/django-cors-headers): middleware de gestion du CORS pour Django
- [psycopg2 2.8](https://github.com/psycopg/psycopg2): pilote PostgreSQL pour Python
- [python-dotenv 0.15](https://github.com/theskumar/python-dotenv): permet de lire des variables d'environnement dans un fichier .env
- [pika 1.1](https://github.com/pika/pika): client RabbitMQ pour Python
- [django-keycloak-auth 0.9](https://github.com/marcelo225/django-keycloak-auth): bibliothèque pour autoriser les ressources d'un API Django REST avec les rôles définis dans un service Keycloak

## Variables d'environnement
Le projet s'appuie essentiellement sur des variables d'environnement à définir dans un fichier .env à la racine du projet. Les valeurs à renseigner sont les suivantes :

- **USER_ID** : L'ID de l'utilisateur au sein du container. Sous Linux, doit être égal à l'ID de votre utilisateur courant. Sous Windows, peu importe.
- **GROUP_ID**: L'ID du groupe de l'utilisateur au sein du container. Sous Linux, doit être égal à l'ID du groupe de votre utilisateur courant. Sous Windows, peu importe.
- **POSTGRES_DB**: Le nom de la base de données du microservice
- **POSTGRES_PORT**: Le port d'écoute de la base de données du microservice
- **POSTGRES_HOSTNAME**: Le nom d'hôte du container dans lequel est tourne la base de données du microservice
- **POSTGRES_USER**: Le nom d'utilisateur de la base de données du microservice
- **POSTGRES_PASSWORD**: Le mot de passe de l'utilisateur de la base de données du microservice
- **BROKER_HOST**: L'adresse IP du serveur sur lequel tourne le service RabbitMQ
- **BROKER_PORT**: Le port d'écoute du service RabbitMQ
- **BROKER_VHOST**: L'hôte virtuel du service RabbitMQ à utiliser
- **BROKER_USER**: L'utilisateur du service RabbitMq
- **BROKER_PASSWORD**: Le mot de passe de l'utilisateur du service RabbitMQ
- **PROJECT_NAME**: Le nom du projet Django. **Ne peut pas contenir de tiret bas**.
- **PROJECT_PORT**: Le port d'écoute du projet Django
- **APP_NAME**: Le nom de l'application Django
- **HOSTNAME**: Le nom d'hôte du container. Doit être un nom de domaine valide.
- **DEBUG**: Booléen à renseigner conformément à si l'on est en développement ou en production
- **DJANGO_SECRET_KEY**: La clé secrète du projet Django. Sera renseignée automatiquement à la génération.
- **DJANGO_SUPERUSER_USERNAME**: Le nom d'utilisateur du superutilisateur Django
- **DJANGO_SUPERUSER_PASSWORD**: Le mot de passe du superutilisateur Django
- **DJANGO_SUPERUSER_EMAIL**: L'adresse e-mail du superutilisateur Django
- **SUBSCRIPTIONS**: La liste des topics RabbitMQ suivi par le microservice, séparés les uns des autres par une virgule
- **KEYCLOAK_SERVER_URL**: L'adresse IP du serveur Keycloak
- **KEYCLOAK_REALM**: Le nom du royaume Keycloak
- **KEYCLOAK_CLIENT_ID**: Le nom du client Keycloak correspondant au microservice
- **KEYCLOAK_CLIENT_SECRET_KEY**: La clé secrète du client Keycloak correspondant au microservice

## Générer le projet de base
Lancer la commande suivante à la racine du projet :

```
docker-compose run web bash scaffold.sh
```

## Démarrer le projet
Lancer la commande suivante à la racine du projet :

```
docker-compose up -d
```

## Arrêter le projet
Lancer la commande suivante à la racine du projet :

```
docker-compose down -d
```

## Intégration dans Pycharm

### Configuration de l'interpréteur Python
1. Aller dans File/Settings/Project: madera-starter/Python Interpreter.
2. Cliquer sur l'icône d'engrenage, puis sur "Add".
3. Dans le menu de droite, choisir "Docker Compose".
4. Dans le champ "Configuration file(s)", sélectionner le fichier docker-compose.yml du projet (./docker-compose.yml).
5. Dans le champ "Service", sélectionner "web".
6. Cliquer sur "OK", puis "OK".

### Lancer le projet depuis l'interface graphique
1. Cliquer sur "Edit Configuration".
2. Dans le menu de gauche, cliquer sur "+".
3. Donner un nom (champ "Name") à la configuration.
4. Dans le champ "Configuration file(s)", sélectionner le fichier docker-compose.yml du projet (./docker-compose.yml).
5. Cliquer sur "OK".
 
